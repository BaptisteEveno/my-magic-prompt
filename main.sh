#!/bin/bash

# Global Variables
PROMPT_VERSION="1.0"
LOGIN="baba"
PASSWORD="password"
FIRST_NAME="Baptiste"
LAST_NAME="Eveno"
EMAIL="baptiste.eveno35@gmail.com"
AGE="24"
LOGGED_IN=false

# History configuration
HISTFILE=~/.custom_prompt_history
HISTSIZE=1000
HISTFILESIZE=2000

# Load the history from HISTFILE
if [[ -f $HISTFILE ]]; then
    history -r
fi

# Function to display help
function show_help() {
    printf "Available commands:\n"
    printf "help       : Show this help message\n"
    printf "ls         : List files and directories, including hidden ones\n"
    printf "rm <file>  : Remove a file\n"
    printf "rmd <dir>  : Remove a directory\n"
    printf "rmdirwtf <dir> : Remove directories with password protection\n"
    printf "about      : Show description of the program\n"
    printf "version    : Show version of the prompt\n"
    printf "age        : Ask for your age and determine if you are a minor or an adult\n"
    printf "quit       : Exit the prompt\n"
    printf "profil     : Show all your profile information\n"
    printf "passw      : Change the password\n"
    printf "cd <dir>   : Change directory\n"
    printf "pwd        : Show current directory\n"
    printf "hour       : Show current time\n"
    printf "httpget    : Download HTML source of a webpage to a file\n"
    printf "smtp       : Send an email\n"
    printf "open <file>: Open a file in VIM\n"
    printf "touch <file>: Create an empty file\n"
    printf "mkdir <dir>: Create a new directory\n"
    printf "rps        : Play a game of Rock Paper Scissors\n"
    printf "*          : Handle unknown commands\n"
}

# Function to authenticate user
function authenticate() {
    local login_attempt password_attempt
    printf "Login: "
    read -r login_attempt
    printf "Password: "
    read -rs password_attempt
    printf "\n"

    if [[ "$login_attempt" == "$LOGIN" && "$password_attempt" == "$PASSWORD" ]]; then
        LOGGED_IN=true
    else
        printf "Authentication failed.\n" >&2
        exit 1
    fi
}

# Function to list files and directories
function list_files() {
    ls -a
}

# Function to remove a file
function remove_file() {
    local file="$1"
    if [[ -z "$file" ]]; then
        printf "Please specify a file to remove.\n" >&2
        return 1
    fi
    rm -i -- "$file"
}

# Function to remove a directory
function remove_directory() {
    local dir="$1"
    if [[ -z "$dir" ]]; then
        printf "Please specify a directory to remove.\n" >&2
        return 1
    fi
    rm -ri -- "$dir"
}

# Function to remove directories with password protection
function remove_directory_wtf() {
    local dir password_attempt
    printf "Enter password: "
    read -rs password_attempt
    printf "\n"

    if [[ "$password_attempt" != "$PASSWORD" ]]; then
        printf "Incorrect password.\n" >&2
        return 1
    fi

    for dir in "$@"; do
        if [[ -z "$dir" ]]; then
            printf "Please specify a directory to remove.\n" >&2
            return 1
        fi
        rm -rf -- "$dir" && printf "Directory removed: %s\n" "$dir"
    done
}

# Function to show about information
function show_about() {
    printf "This is a custom shell prompt program created for demonstration purposes.\n"
}

# Function to show version
function show_version() {
    printf "Prompt Version: %s\n" "$PROMPT_VERSION"
}

# Function to ask and check age
function check_age() {
    local age
    printf "Enter your age: "
    read -r age
    if [[ "$age" -ge 18 ]]; then
        printf "You are an adult.\n"
    else
        printf "You are a minor.\n"
    fi
}

# Function to show profile information
function show_profile() {
    printf "First Name: %s\n" "$FIRST_NAME"
    printf "Last Name : %s\n" "$LAST_NAME"
    printf "Age       : %s\n" "$AGE"
    printf "Email     : %s\n" "$EMAIL"
}

# Function to change password
function change_password() {
    local old_password new_password confirm_password
    printf "Enter current password: "
    read -rs old_password
    printf "\n"
    if [[ "$old_password" != "$PASSWORD" ]]; then
        printf "Incorrect current password.\n" >&2
        return 1
    fi
    printf "Enter new password: "
    read -rs new_password
    printf "\n"
    printf "Confirm new password: "
    read -rs confirm_password
    printf "\n"
    if [[ "$new_password" == "$confirm_password" ]]; then
        PASSWORD="$new_password"
        printf "Password changed successfully.\n"
    else
        printf "Passwords do not match.\n" >&2
        return 1
    fi
}

# Function to change directory
function change_directory() {
    local dir="$1"
    if [[ -z "$dir" ]]; then
        printf "Please specify a directory to change to.\n" >&2
        return 1
    fi
    if cd -- "$dir"; then
        printf "Changed to directory: %s\n" "$dir"
    else
        printf "Failed to change directory.\n" >&2
        return 1
    fi
}

# Function to show current directory
function show_current_directory() {
    pwd
}

# Function to show current time
function show_current_time() {
    date +"%T"
}

# Function to handle unknown commands
function unknown_command() {
    printf "Unknown command: %s\n" "$1" >&2
}

# Function to download HTML source of a webpage
function download_html() {
    local url filename
    printf "Enter the URL: "
    read -r url
    printf "Enter the filename to save as: "
    read -r filename
    if ! curl -o "$filename" "$url"; then
        printf "Failed to download the webpage.\n" >&2
        return 1
    fi
    printf "Webpage saved as %s\n" "$filename"
}

# Function to send an email
function send_email() {
    local to subject body
    printf "Enter recipient email: "
    read -r to
    printf "Enter subject: "
    read -r subject
    printf "Enter body of the email: "
    read -r body
    if ! printf "%s" "$body" | mail -s "$subject" "$to"; then
        printf "Failed to send the email.\n" >&2
        return 1
    fi
    printf "Email sent successfully to %s\n" "$to"
}

# Function to open a file in VIM
function open_file() {
    local file="$1"
    if [[ -z "$file" ]]; then
        printf "Please specify a file to open.\n" >&2
        return 1
    fi
    vim -- "$file"
}

# Function to create a new file
function create_file() {
    local file="$1"
    if [[ -z "$file" ]]; then
        printf "Please specify a file name to create.\n" >&2
        return 1
    fi
    touch -- "$file" && printf "File created: %s\n" "$file"
}

# Function to create a new directory
function create_directory() {
    local dir="$1"
    if [[ -z "$dir" ]]; then
        printf "Please specify a directory name to create.\n" >&2
        return 1
    fi
    mkdir -p -- "$dir" && printf "Directory created: %s\n" "$dir"
}

# Function to play Rock Paper Scissors
function play_rps() {
    local p1 p2 p1_choice p2_choice p1_score=0 p2_score=0 round=1
    printf "Enter name for Player 1: "
    read -r p1
    printf "Enter name for Player 2: "
    read -r p2

    while [[ $p1_score -lt 3 && $p2_score -lt 3 ]]; do
        printf "\nRound %d\n" "$round"
        printf "%s's turn (rock, paper, scissors, SuperKitty): " "$p1"
        read -r p1_choice
        printf "%s's turn (rock, paper, scissors): " "$p2"
        read -r p2_choice

        if [[ "$p1_choice" == "SuperKitty" ]]; then
            printf "%s wins the round with SuperKitty!\n" "$p1"
            ((p1_score++))
        elif [[ "$p1_choice" == "$p2_choice" ]]; then
            printf "It's a tie!\n"
        elif [[ ("$p1_choice" == "rock" && "$p2_choice" == "scissors") ||
                ("$p1_choice" == "paper" && "$p2_choice" == "rock") ||
                ("$p1_choice" == "scissors" && "$p2_choice" == "paper") ]]; then
            printf "%s wins the round!\n" "$p1"
            ((p1_score++))
        else
            printf "%s wins the round!\n" "$p2"
            ((p2_score++))
        fi

        printf "Score: %s %d - %d %s\n" "$p1" "$p1_score" "$p2_score" "$p2"
        ((round++))
    done

    if [[ $p1_score -eq 3 ]]; then
        printf "%s wins the game!\n" "$p1"
    else
        printf "%s wins the game!\n" "$p2"
    fi
}

# Main function
function main() {
    authenticate

    while $LOGGED_IN; do
        local prompt_dir
        prompt_dir=$(pwd)
        printf "%s> " "$prompt_dir"
        read -e -r command args
        history -s "$command $args"
        case $command in
            help) show_help ;;
            ls) list_files ;;
            rm) remove_file "$args" ;;
            rmd|rmdir) remove_directory "$args" ;;
            rmdirwtf) remove_directory_wtf "$args" ;;
            about) show_about ;;
            version|--v|vers) show_version ;;
            age) check_age ;;
            quit) LOGGED_IN=false ;;
            profil) show_profile ;;
            passw) change_password ;;
            cd) change_directory "$args" ;;
            pwd) show_current_directory ;;
            hour) show_current_time ;;
            httpget) download_html ;;
            smtp) send_email ;;
            open) open_file "$args" ;;
            touch) create_file "$args" ;;
            mkdir) create_directory "$args" ;;
            rps) play_rps ;;
            *) unknown_command "$command" ;;
        esac
    done

    # Save the history
    history -w
}

main
