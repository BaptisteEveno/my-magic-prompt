# Custom Shell Prompt

Ce projet est un script Bash personnalisé qui implémente un shell interactif avec diverses commandes utiles.

## Fonctionnalités

- **touch <file>** : Crée un fichier vide.
- **mkdir <dir>** : Crée un nouveau répertoire.
- **help** : Affiche la liste des commandes disponibles.
- **ls** : Liste les fichiers et répertoires, y compris les fichiers cachés.
- **rm <file>** : Supprime un fichier.
- **rmd <dir>** : Supprime un répertoire.
- **rmdirwtf <dir>** : Supprime des répertoires avec une protection par mot de passe.
- **about** : Affiche une description du programme.
- **version** : Affiche la version du script.
- **age** : Demande votre âge et détermine si vous êtes majeur ou mineur.
- **quit** : Quitte le shell.
- **profil** : Affiche toutes les informations de profil de l'utilisateur.
- **passw** : Change le mot de passe avec confirmation.
- **cd <dir>** : Change de répertoire.
- **pwd** : Affiche le répertoire courant.
- **hour** : Affiche l'heure actuelle.
- **httpget** : Télécharge le code source HTML d'une page web et l'enregistre dans un fichier.
- **smtp** : Envoie un email.
- **open <file>** : Ouvre un fichier dans l'éditeur VIM.
- **rps** : Joue à un jeu de Pierre-Papier-Ciseaux à deux joueurs.
- **\*** : Gère les commandes inconnues.

## Installation

1. Clonez le dépôt ou copiez le script dans un fichier nommé `main.sh`.
2. Rendez le script exécutable :
    ```bash
    chmod +x main.sh
    ```
3. Exécutez le script :
    ```bash
    ./main.sh
    ```

## Utilisation

Après avoir lancé le script, vous serez invité à vous authentifier en entrant un login et un mot de passe. Les identifiants par défaut sont :

- **Login** : `baba`
- **Password** : `password`


## Historique des commandes

Le script maintient un historique des commandes entrées. Vous pouvez utiliser les flèches haut et bas pour naviguer dans l'historique des commandes. L'historique est enregistré dans le fichier `~/.custom_prompt_history`.

## Configuration

Vous pouvez modifier les informations de l'utilisateur et les paramètres de configuration en modifiant les variables globales en haut du script `main.sh`.


## Commandes spéciales

### rps : Jeu de Pierre-Papier-Ciseaux à deux joueurs

Cette commande permet à deux joueurs de jouer à Pierre-Papier-Ciseaux avec les fonctionnalités suivantes :
- Demander le nom du Joueur 1 et du Joueur 2.
- Afficher le nom du joueur qui doit jouer à chaque tour.
- Déterminer un vainqueur en 3 manches.
- Compter les points.
- Inclure un pouvoir spécial "SuperKitty" pour gagner à tous les coups.

Pour jouer, tapez :
```bash
rps
```
